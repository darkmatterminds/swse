-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

local tokenref = nil;

function onInit()
	-- Acquire token reference, if any
	linkToken();
	
	-- Update the wound and status displays
	onActiveChanged();
	onFactionChanged();
	onTypeChanged();
	onWoundsChanged();
	
	-- Track the effects list
	local node_list_effects = NodeManager.createChild(getDatabaseNode(), "effects");
	if node_list_effects then
		node_list_effects.onChildUpdate = onEffectsChanged;
		node_list_effects.onChildAdded = onEffectsChanged;
	end
	onEffectsChanged();
end

function linkToken()
	if tokenrefid and tokenrefnode then
		local imageinstance = token.populateFromImageNode(tokenrefnode.getValue(), tokenrefid.getValue());
		if imageinstance then
			tokenref = imageinstance;
			tokenref.onDelete = refDeleted;
			tokenref.onDrop = refDrop;
		end
	end
end

function refDeleted(deleted)
	tokenref = nil;
end

function refDrop(droppedon, draginfo)
	CombatCommon.onDrop("ct", getDatabaseNode().getNodeName(), draginfo);
end

function updateDisplay()
	if active.getValue() == 1 then
		name.setFont("ct_active");

		active_spacer_top.setVisible(true);
		active_spacer_bottom.setVisible(true);
		
		local sFaction = friendfoe.getValue();
		if sFaction == "friend" then
			setFrame("ctentrybox_friend_active");
		elseif sFaction == "neutral" then
			setFrame("ctentrybox_neutral_active");
		elseif sFaction == "foe" then
			setFrame("ctentrybox_foe_active");
		else
			setFrame("ctentrybox_active");
		end
	else
		name.setFont("ct_name");

		active_spacer_top.setVisible(false);
		active_spacer_bottom.setVisible(false);
		
		local sFaction = friendfoe.getValue();
		if sFaction == "friend" then
			setFrame("ctentrybox_friend");
		elseif sFaction == "neutral" then
			setFrame("ctentrybox_neutral");
		elseif sFaction == "foe" then
			setFrame("ctentrybox_foe");
		else
			setFrame("ctentrybox");
		end
	end
end

function onActiveChanged()
	-- Update the active icon
	active_icon.setVisible(active.getValue() ~= 0);
	
	-- Update the display
	updateDisplay();
end

function onFactionChanged()
	-- Update the faction icon
	friendfoe_icon.updateIcon(friendfoe.getValue());
	
	-- Update the display
	updateDisplay();
end

function onTypeChanged()
	-- Update what fields are visible for health display
	updateHealthDisplay();
end

function onWoundsChanged()
	-- Calculate the percent wounded for this unit
	local percent_wounded = 0;
	if hp.getValue() > 0 then
		percent_wounded = wounds.getValue() / hp.getValue();
	end
	
	-- Based on the percent wounded, change the font color for the Wounds field
	if percent_wounded <= 0 then
		wounds.setFont("ct_healthy_number");
	elseif percent_wounded < .5 then
		wounds.setFont("ct_ltwound_number");
	elseif percent_wounded < 1 then
		wounds.setFont("ct_bloodied_number");
	else
		wounds.setFont("ct_dead_number");
	end
end

function onSurgesChanged()
	-- Update the healing surges remaining field when healing surges max or healing surges used changes
	if type.getValue() == "pc" then
		healsurgeremaining.setValue(healsurgesmax.getValue() - healsurgesused.getValue());
	end
end

function onEffectsChanged()
	-- Rebuild the effects list
	local affectedby = EffectsManager.getEffectsString(getDatabaseNode());
	
	-- Update the effects line in the client combat tracker
	if affectedby == "" then
		effects_label.setVisible(false);
		effects_str.setVisible(false);
	else
		effects_label.setVisible(true);
		effects_str.setVisible(true);
	end
	effects_str.setValue(affectedby);
end

-- Section visibility handling

function updateHealthDisplay()
	-- Check the party health view option
	-- Hide any NPC health fields, and if the option is off, hide the party health fields also
	if type.getValue() == "npc" or OptionsManager.isOption("SHPH", "off") then
		hp.setVisible(false);
		hptemp.setVisible(false);
		wounds.setVisible(false);
		healsurgeremaining.setVisible(false);

		status.setVisible(true);
	else
		hp.setVisible(true);
		hptemp.setVisible(true);
		wounds.setVisible(true);
		healsurgeremaining.setVisible(true);

		status.setVisible(false);
	end
end
