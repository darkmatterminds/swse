-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	if not getNextWindow(nil) then
		setVisible(false);
	end
end

function onSortCompare(w1, w2)
	local name1 = w1.name.getValue();
	local name2 = w2.name.getValue();

	if name1 == "" then
		return true;
	elseif name2 == "" then
		return false;
	else
		return name1 > name2;
	end
end

function onFilter(w)
	-- We always assume combat mode for the mini sheet
	-- which means that only available powers are to be displayed
	
	-- If we're over the individual power limit, then hide
	local used = NodeManager.get(w.getDatabaseNode(), "used", 0);
	local max = NodeManager.get(w.getDatabaseNode(), "prepared", 1);
	if used >= max then
		return false;
	end
	
	-- Otherwise, show the power
	return true;
end
