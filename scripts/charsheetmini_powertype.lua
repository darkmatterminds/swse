-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	-- Set the label to the correct value
	local shownode = nil;
	local nodename = getDatabaseNode().getName();
	if nodename == "a1-atwill" then
		label.setValue("At-Will");
	elseif nodename == "a0-situational" then
		label.setValue("Situational");
	elseif nodename == "a11-atwillspecial" then
		label.setValue("At-Will (Special)");
		shownode = NodeManager.createChild(getDatabaseNode(), "...powershow.atwillspecial", "number");
	elseif nodename == "a2-cantrip" then
		label.setValue("Cantrips");
		shownode = NodeManager.createChild(getDatabaseNode(), "...powershow.cantrip", "number");
	elseif nodename == "b1-encounter" then
		label.setValue("Encounter");
	elseif nodename == "b11-encounterspecial" then
		label.setValue("Encounter (Special)");
		shownode = NodeManager.createChild(getDatabaseNode(), "...powershow.encounterspecial", "number");
	elseif nodename == "b2-channeldivinity" then
		label.setValue("Channel Divinity");
		shownode = NodeManager.createChild(getDatabaseNode(), "...powershow.channeldivinity", "number");
	elseif nodename == "c-daily" then
		label.setValue("Daily");
	elseif nodename == "d-utility" then
		label.setValue("Utility");
	elseif nodename == "e-itemdaily" then
		label.setValue("Item (Daily)");
	end
	
	-- Notify of mode change to make sure everything is set up right
	if shownode then
		shownode.onUpdate = onShowChange;
		onShowChange(shownode);
	end
end

function getPowersUsed()
	local powersused = 0;
	for k,v in pairs(powerlist.getWindows()) do
		powersused = powersused + v.usedcounter.getValue();
	end
	return powersused;
end

function getAvailablePowerCount()
	-- Cycle through each power, counting the ones used and available
	local powercount = 0;
	local powerused = 0;
	for k,v in pairs(powerlist.getWindows()) do
		if v.usedcounter.getValue() < v.usedcounter.getMaxValue() then
			powercount = powercount + 1;
		end
		powerused = powerused + v.usedcounter.getValue();
	end
	
	-- If this is a limited use power type, then we have zero powers if we are over the uses available
	local nodename = getDatabaseNode().getName();
	if nodename == "b2-channeldivinity" or nodename == "c-daily" or nodename == "d-utility" or nodename == "e-itemdaily" then
		local uses = usesavailable.getValue();
		if uses > 0 and (powerused >= uses) then
			powercount = 0;
		end
	end
	
	-- Return the power count
	return powercount;
end

function onPowerClick(state)
	checkUseLimit();

	powerlist.applyFilter();
end

function checkUseLimit()
	local count = getAvailablePowerCount();

	-- Go through each power in this type to make sure it's active state is right
	for k,v in pairs(powerlist.getWindows()) do
		if count > 0 then
			v.usedcounter.enable();
		else
			v.usedcounter.disable();
		end
	end
end

function onShowChange(source)
	-- Set the visibility based on the checkbox it is connected to
	setVisible(source.getValue());
end

function setVisible(value)
	if value == 1 or value == true then
		label.setVisible(true);
		powerlist.setVisible(true);
	else
		label.setVisible(false);
		powerlist.setVisible(false);
	end
end
