-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	NodeManager.createChild(getDatabaseNode(), "a0-situational");
	NodeManager.createChild(getDatabaseNode(), "a1-atwill");
	NodeManager.createChild(getDatabaseNode(), "a11-atwillspecial");
	NodeManager.createChild(getDatabaseNode(), "a2-cantrip");
	NodeManager.createChild(getDatabaseNode(), "b1-encounter");
	NodeManager.createChild(getDatabaseNode(), "b11-encounterspecial");
	NodeManager.createChild(getDatabaseNode(), "b2-channeldivinity");
	NodeManager.createChild(getDatabaseNode(), "c-daily");
	NodeManager.createChild(getDatabaseNode(), "d-utility");
	NodeManager.createChild(getDatabaseNode(), "e-itemdaily");
	
	CharSheetCommon.checkForSecondWind(window.getDatabaseNode());
end

function onSortCompare(w1, w2)
	local name1 = w1.getDatabaseNode().getName();
	local name2 = w2.getDatabaseNode().getName();

	if name1 == "" then
		return true;
	elseif name2 == "" then
		return false;
	else
		return name1 > name2;
	end
end

function getMode()
	return window.powermode.getStringValue();
end

function onFilter(w)
	-- Apply the filter to the individual powers too
	w.powerlist.applyFilter();
	
	-- If we are entering preparation mode, then hide the power types that have no
	-- preparation component
	local mode = getMode();
	if mode == "preparation" then
		local label = w.label.getValue();
		if label == "At-Will (Special)" or label == "Encounter (Special)" then
			return true;
		elseif (label == "Daily" or label == "Utility") and (w.getUsesAvailable() > 0) then
			return true;
		end
		
		return false;

	-- If combat mode, then hide the power types with no options left
	elseif mode == "combat" then
		local powercount = w.getAvailablePowerCount();
		if powercount <= 0 then
			return false;
		end
	end

	return true;
end

function onDrop(x, y, draginfo)
	if draginfo.isType("shortcut") then
		local class, datasource = draginfo.getShortcutData();
		if class == "powerdesc" or class == "reference_power_custom" or class == "reference_npcaltpower" then
			addPower(draginfo.getDatabaseNode(), class);
		elseif class == "reference_magicitem_property" then
			local nodePower = addPower(draginfo.getDatabaseNode(), class);
			if nodePower then
				local sItem = NodeManager.get(draginfo.getDatabaseNode(), "...name", "");
				local sEnhancement = "";
				if string.match(sItem, " %+%d$") then
					sEnhancement = string.sub(sItem, -2);
					sItem = string.sub(sItem, 1, -4);
				end
				local sName = "Property";
				if sItem ~= "" then
					sName = sItem .. " - " .. sName;
				end
				NodeManager.set(nodePower, "name", "string", sName);
				NodeManager.set(nodePower, "source", "string", "Item");
				
				if sEnhancement ~= "" then
					local sDescription = NodeManager.get(nodePower, "shortdescription", "");
					sDescription = string.gsub(sDescription, "equal to .* enhancement bonus%.", "equal to " .. sEnhancement .. ".");
					NodeManager.set(nodePower, "shortdescription", "string", sDescription);
				end
			end
		end
		
		return true;
	end
end

function addPower(sourcenode, sNodeClass)
	-- Parameter validation
	if not sourcenode then
		return;
	end
	
	-- Add the power to the chaarcter database
	local nodePower, powertype = CharSheetCommon.addPowerDB(getDatabaseNode().getParent(), sourcenode, sNodeClass)

	-- Change the appropriate list state to visible, since we just added a power
	for k,v in pairs(getWindows()) do
		if v.getDatabaseNode().getName() == powertype then
			v.forceActive();
		end
	end

	if sNodeClass == "powerdesc" then
		-- Add any other powers linked to this power
		if sourcenode.getChild("linkedpowers") then
			for k,v in pairs(sourcenode.getChild("linkedpowers").getChildren()) do
				local powerclass, powernodename = v.getChild("link").getValue();
				if powerclass == "powerdesc" then
					addPower(DB.findNode(powernodename), powerclass);
				end
			end
		elseif sourcenode.getChild("link") then
			local powerclass, powernodename = sourcenode.getChild("link").getValue();
			if powerclass == "powerdesc" then
				addPower(DB.findNode(powernodename), powerclass);
			end
		end
	end
	
	-- Return power created
	return nodePower;
end
