-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	if link and linkedpowername then
		local linknode = link.getTargetDatabaseNode();
		if linknode then
			linkedpowername.setValue(NodeManager.get(linknode, "name", ""));
		end
	end

	self.onSizeChanged = update;
end

function update(wnd)
	local h = 0;
	if recharge then
		_,h = recharge.getSize();
	end
	if h == 0 then
		return;
	end
	
	-- UNREGISTER UPDATE, OTHERWISE GET ENDLESS LOOP
	self.onSizeChanged = function () end;
	
	local nLineHeight = 0;
	local nLineOffset = 5;
	local nTopOffset = 0;
	
	if stringflavor then
		local bFlavor = (stringflavor.getValue() ~= "");

		stringflavor.setVisible(bFlavor);
		if bFlavor then
			_,h = stringflavor.getSize();
			nLineHeight = math.max(h, nLineHeight);
		end
	end
	
	local sRWAnchor = "";
	if recharge and diamond and keywords then
		if nLineHeight > 0 then
			nTopOffset = nTopOffset + nLineHeight + nLineOffset;
			nLineHeight = 0;
		end

		local bKeywords = (keywords.getValue() ~= "");
		local bRecharge = (recharge.getValue() ~= "");

		keywords.setVisible(bKeywords);
		if bKeywords then
			_,h = keywords.getSize();
			sRWAnchor = "keywords";
			nLineHeight = math.max(h, nLineHeight);
		end
		
		recharge.setVisible(bRecharge);
		if bRecharge then
			_,h = recharge.getSize();
			if h > nLineHeight then
				nLineHeight = h;
				sRWAnchor = "recharge";
			elseif sRWAnchor == "" then
				sRWAnchor = "recharge";
			end
		end

		diamond.setVisible(bRecharge and bKeywords);
	end
	
	local sARAnchor = "";
	if action and range then
		if nLineHeight > 0 then
			nTopOffset = nTopOffset + nLineHeight + nLineOffset;
			nLineHeight = 0;
		end

		local bAction = (action.getValue() ~= "");
		local bRange = (range.getValue() ~= "");

		range.setVisible(bRange);
		if sRWAnchor == "" then
			range.setAnchor("top", "", "top", "absolute", nTopOffset);
		else
			range.setAnchor("top", sRWAnchor, "bottom", "absolute", nLineOffset);
		end
		if bRange then
			_,h = range.getSize();
			sARAnchor = "range";
			nLineHeight = math.max(h, nLineHeight);
		end

		action.setVisible(bAction);
		if sRWAnchor == "" then
			action.setAnchor("top", "", "top", "absolute", nTopOffset);
		else
			action.setAnchor("top", sRWAnchor, "bottom", "absolute", nLineOffset);
		end
		if bAction then
			_,h = action.getSize();
			if h > nLineHeight then
				nLineHeight = h;
				sARAnchor = "action";
			elseif sRWAnchor == "" then
				sARAnchor = "action";
			end
		end
	end
	
	if shortdescription then
		if nLineHeight > 0 then
			nTopOffset = nTopOffset + nLineHeight + nLineOffset;
			nLineHeight = 0;
		end

		if sARAnchor == "" then
			shortdescription.setAnchor("top", "", "top", "absolute", nTopOffset);
		else
			shortdescription.setAnchor("top", sARAnchor, "bottom", "absolute", nLineOffset);
		end
	end
	if description then
		if nLineHeight > 0 then
			nTopOffset = nTopOffset + nLineHeight + nLineOffset;
			nLineHeight = 0;
		end

		if sARAnchor == "" then
			description.setAnchor("top", "", "top", "absolute", nTopOffset);
		else
			description.setAnchor("top", sARAnchor, "bottom", "absolute", nLineOffset);
		end
	end
end
