-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

iscustom = true;
sets = {};

function onInit()
	setRadialDeleteOption();

	onStatNameUpdate();
end

function onMenuSelection(item)
	if item == 6 then
		getDatabaseNode().delete();
	end
end

function onStatNameUpdate()
	stat.update(statname.getStringValue());

	if StringManager.contains({"strength", "dexterity", "constitution"}, statname.getStringValue()) then
		armorwidget.setIcon("indicator_armorcheck");
	else
		armorwidget.setIcon(nil);
	end
end

-- This function is called to set the entry to non-custom or custom.
-- Custom entries have configurable stats and editable labels.
function setCustom(state)
	iscustom = state;
	
	if not iscustom then
		label.setEnabled(false);
		label.setFrame(nil);
		
		statname.setStateFrame("hover", nil);
		statname.setReadOnly(true);
	end
	
	setRadialDeleteOption();
end

function setRadialDeleteOption()
	resetMenuItems();

	if iscustom then
		registerMenuItem("Delete", "delete", 6);
	end
end

