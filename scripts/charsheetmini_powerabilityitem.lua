-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function isWeaponPower()
	local keywords = string.lower(NodeManager.get(getDatabaseNode(), "...keywords", ""));
	local isWeapon = false;
	if string.match(keywords, "weapon") then
		isWeapon = true;
	end
	return isWeapon;
end

function highlight(bState)
	if bState then
		setFrame("rowshade");
	else
		setFrame(nil);
	end
end

function onFilter()
	local abilitytype = type.getStringValue();
	local abilitynode = getDatabaseNode();

	-- Determine some details about the ability
	local isAttack = false;
	local isHeal = false;
	local isEffect = false;
	if abilitytype == "attack" then
		isAttack = true;
	elseif abilitytype == "heal" then
		isHeal = true;
	elseif abilitytype == "effect" then
		isEffect = true;
	end
	local isWeapon = isWeaponPower();
	
	-- Handle the focus elements of this attack (weapon / implement)
	local showfocus = (focus.getValue() ~= 0);

	-- Handle the attack elements
	local showattack = false;
	showattack = (NodeManager.get(abilitynode, "attackstat", "") ~= "") or showattack;
	showattack = (NodeManager.get(abilitynode, "attackstatmodifier", 0) ~= 0) or showattack;
	showattack = (NodeManager.get(abilitynode, "attackdef", "") ~= "") or showattack;

	-- Handle the damage elements
	local showdamage = false;
	if isWeapon then
		showdamage = (NodeManager.get(abilitynode, "damageweaponmult", 0) > 0) or showdamage;
	end
	local tempdice = NodeManager.get(abilitynode, "damagebasicdice", {});
	showdamage = (#tempdice > 0) or showdamage;
	showdamage = (NodeManager.get(abilitynode, "damagestat", "") ~= "") or showdamage;
	showdamage = (NodeManager.get(abilitynode, "damagestat2", "") ~= "") or showdamage;
	showdamage = (NodeManager.get(abilitynode, "damagestatmodifier", 0) ~= 0) or showdamage;

	-- Handle the heal elements
	local showheal = false;
	showheal = (NodeManager.get(abilitynode, "hsvmult", 0) > 0) or showheal;
	tempdice = NodeManager.get(abilitynode, "healdice", {});
	showheal = (#tempdice > 0) or showheal;
	showheal = (NodeManager.get(abilitynode, "healstat", "") ~= "") or showheal;
	showheal = (NodeManager.get(abilitynode, "healstat2", "") ~= "") or showheal;
	showheal = (NodeManager.get(abilitynode, "healmod", 0) ~= 0) or showheal;
	showheal = (NodeManager.get(abilitynode, "healcost", 0) ~= 0) or showheal;

	-- Handle the effect elements
	local showeffect = false;
	showeffect = (NodeManager.get(abilitynode, "label", "") ~= "") or showeffect;

	---- ATTACK ----
	focuslabel.setVisible(isAttack and showfocus);
	focus.setVisible(isAttack and showfocus);
	if isWeapon then
		focuslabel.setValue("Wpn:");
	else
		focuslabel.setValue("Imp:");
	end
	attacklabel.setVisible(isAttack and showattack);
	attackicon.setVisible(isAttack and showattack);
	damagelabel.setVisible(isAttack and showdamage);
	damageicon.setVisible(isAttack and showdamage);

	---- HEAL ----
	heallabel.setVisible(isHeal and showheal);
	healicon.setVisible(isHeal and showheal);
	
	---- EFFECT ----
	effectview.setVisible(isEffect and showeffect);
	
	if abilitytype == "attack" then
		-- Return visibility depending on if any fields are visible
		return (showfocus or showattack or showdamage);
	
	elseif abilitytype == "heal" then
		return showheal;
		
	elseif abilitytype == "effect" then
		return showeffect;
	end
	
	-- If no type chosen yet, then always hide
	return false;
end

