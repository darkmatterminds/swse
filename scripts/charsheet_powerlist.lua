-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

disablecheck = false;

function onInit()
	if not getNextWindow(nil) then
		setVisible(false);
	end
end

function checkForEmpty()
	super.checkForEmpty();
	window.checkUseLimit();
end

function onSortCompare(w1, w2)
	local name1 = w1.name.getValue();
	local name2 = w2.name.getValue();

	if name1 == "" then
		return true;
	elseif name2 == "" then
		return false;
	else
		return name1 > name2;
	end
end

function onFilter(w)
	-- Update usage display
	w.updateUsageDisplay();
	
	-- If we're in combat mode then hide the powers that are unavailable
	local mode = window.windowlist.getMode();
	if mode == "combat" then
		if w.usedcounter.getValue() >= w.usedcounter.getMaxValue() then
			return false;
		end
	end
	
	return true;
end

function onDrop(x, y, draginfo)
	if draginfo.isType("shortcut") then
		local class, datasource = draginfo.getShortcutData();
		if class == "powerdesc" or class == "reference_power_custom" or class == "reference_npcaltpower" then
			addPower(draginfo.getDatabaseNode(), class);
		elseif class == "reference_magicitem_property" then
			local nodePower = addPower(draginfo.getDatabaseNode(), class);
			if nodePower then
				local sItem = NodeManager.get(draginfo.getDatabaseNode(), "...name", "");
				local sEnhancement = "";
				if string.match(sItem, " %+%d$") then
					sEnhancement = string.sub(sItem, -2);
					sItem = string.sub(sItem, 1, -4);
				end
				local sName = "Property";
				if sItem ~= "" then
					sName = sItem .. " - " .. sName;
				end
				NodeManager.set(nodePower, "name", "string", sName);
				NodeManager.set(nodePower, "source", "string", "Item");
				
				if sEnhancement ~= "" then
					local sDescription = NodeManager.get(nodePower, "shortdescription", "");
					sDescription = string.gsub(sDescription, "equal to .* enhancement bonus%.", "equal to " .. sEnhancement .. ".");
					NodeManager.set(nodePower, "shortdescription", "string", sDescription);
				end
			end
		end

		return true;
	end
end

function onEnter()
	local wnd = NodeManager.createWindow(self);
	if wnd then
		wnd.name.setFocus();
	end
end

function onCheckDeletePermission(w)
	if #getWindows() == 1 then
		setVisible(false);
		window.icon.setState(false);
	end
	
	return true;
end

function addPower(sourcenode, sNodeClass)
	-- Parameter validation
	if not sourcenode then
		return;
	end
	
	-- Add the power to the chaarcter database
	local nodePower = CharSheetCommon.addPowerDB(getDatabaseNode().getParent().getParent().getParent(), sourcenode, sNodeClass, getDatabaseNode().getParent().getName());

	-- Change our list state to visible, since we just added a power
	window.forceActive();
	
	if sNodeClass == "powerdesc" then
		-- Add any other powers linked to this power
		if sourcenode.getChild("linkedpowers") then
			for k,v in pairs(sourcenode.getChild("linkedpowers").getChildren()) do
				local powerclass, powernodename = v.getChild("link").getValue();
				if powerclass == "powerdesc" then
					window.windowlist.addPower(DB.findNode(powernodename), powerclass);
				end
			end
		elseif sourcenode.getChild("link") then
			local powerclass, powernodename = sourcenode.getChild("link").getValue();
			if powerclass == "powerdesc" then
				window.windowlist.addPower(DB.findNode(powernodename), powerclass);
			end
		end
	end
	
	-- Return power just created
	return nodePower;
end
