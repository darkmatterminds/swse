-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function addMIWeapon(source)
	-- Add the data to the magic item
	local statwindow = window.getDatabaseNode()

	NodeManager.set(statwindow, "damage", "string", NodeManager.get(source, "damage", "") .. " damage");
	NodeManager.set(statwindow, "profbonus", "number", NodeManager.get(source, "profbonus", 0));
	NodeManager.set(statwindow, "weight", "number", NodeManager.get(source, "weight", 0));
	NodeManager.set(statwindow, "range", "number", NodeManager.get(source, "range", 0));
	NodeManager.set(statwindow, "group", "string", NodeManager.get(source, "group", ""));
	NodeManager.set(statwindow, "properties", "string", NodeManager.get(source, "properties", ""));
	NodeManager.set(statwindow, "type", "string", NodeManager.get(source, "type", ""));
	
	local srcname = NodeManager.get(source, "name", "");
	local dstname = NodeManager.get(statwindow, "name", "");
	dstname = string.gsub(dstname, " Weapon ", " " .. srcname .. " ");
	NodeManager.set(statwindow, "name", "string", dstname);
end

function addMIArmor(source)
	-- Add the data to the magic item
	local statwindow = window.getDatabaseNode()

	NodeManager.set(statwindow, "ac", "number", NodeManager.get(source, "ac", 0));
	NodeManager.set(statwindow, "min_enhance", "number", NodeManager.get(source, "min_enhance", 0));
	NodeManager.set(statwindow, "weight", "number", NodeManager.get(source, "weight", 0));
	NodeManager.set(statwindow, "checkpenalty", "number", NodeManager.get(source, "checkpenalty", 0));
	NodeManager.set(statwindow, "speed", "number", NodeManager.get(source, "speed", 0));
	NodeManager.set(statwindow, "type", "string", NodeManager.get(source, "type", ""));
	
	local srcname = NodeManager.get(source, "name", "");
	local dstname = NodeManager.get(statwindow, "name", "");
	dstname = string.gsub(dstname, " Armor ", " " .. srcname .. " ");
	NodeManager.set(statwindow, "name", "string", dstname);
end

function addMIEquipment(source)
	-- Add the data to the magic item
	local statwindow = window.getDatabaseNode()

	NodeManager.set(statwindow, "weight", "number", NodeManager.get(source, "weight", 0));
end

function onDrop(x, y, draginfo)
	if User.isHost() then
		if draginfo.isType("shortcut") then
			local class, datasource = draginfo.getShortcutData();
			local source = draginfo.getDatabaseNode();

			if source then
				if class == "referenceweapon" then
					addMIWeapon(source);
				elseif class == "referencearmor" then
					addMIArmor(source);
				elseif class == "referenceequipment" then
					addMIEquipment(source);
				end
			end

			return true;
		end
	end
end

