-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function getTrackerNode()
	return DB.findNode("skillchallenge");
end

function getTrackerSkillsNode()
	return DB.findNode("skillchallenge.skills");
end

function addResult(sText, nResult)
	-- CHECK FOR SKILL ROLL
	local sSkill = string.match(sText, "%[SKILL%] ([^%[]+)");
	if not sSkill then
		return;
	end
	sSkill = StringManager.trimString(sSkill);
	
	-- GET ROLLER NAME
	local sRoller = string.match(sText, "(.+) %-> %[SKILL%]");
	if sRoller then
		if string.match(sRoller, "^%[GM%]") then
			sRoller = string.sub(sRoller, 6); 
		end
		if string.match(sRoller, "^%[TOWER%]") then
			sRoller = string.sub(sRoller, 10); 
		end	
	else
		sRoller = "";
	end
	
	-- ADD SKILL ROLL RESULT TO CORRECT SKILL
	local nodeSkills = getTrackerSkillsNode();
	if nodeSkills then
		for keySkill, nodeSkill in pairs(nodeSkills.getChildren()) do
			if NodeManager.get(nodeSkill, "label", "") == sSkill then
				local nSkillDC = NodeManager.get(nodeSkill, "DC", 0);
				if nSkillDC > 0 then
					local nodeRolls = nodeSkill.createChild("rolls");
					if nodeRolls then
						local nodeRoll = nodeRolls.createChild();
						if nodeRoll then
							NodeManager.set(nodeRoll, "actor", "string", sRoller);
							NodeManager.set(nodeRoll, "roll", "number", nResult);
							if nResult >= nSkillDC then
								NodeManager.set(nodeRoll, "result", "string", "success");
							else
								NodeManager.set(nodeRoll, "result", "string", "failure");
							end
						end
					end
				end
				
				break;
			end
		end
	end
	
	-- UPDATE TOTALS
	calcTotals();
end

function addDC(sSkill, nDC)	
	local nodeSkills = getTrackerSkillsNode();
	if nodeSkills then
		for keySkill, nodeSkill in pairs(nodeSkills.getChildren()) do
			if NodeManager.get(nodeSkill, "label", "") == sSkill then
				NodeManager.set(nodeSkill, "DC", "number", nDC);
				break;
			end
		end
	end
	
	calcTotals();
end

function calcTotals()
	-- SETUP
	local nTotalSuccess = 0;
	local nTotalFailure = 0;
	
	-- ITERATE THROUGH SKILLS
	local nodeSkills = getTrackerSkillsNode();
	if nodeSkills then
		for keySkill, nodeSkill in pairs(nodeSkills.getChildren()) do
			-- SKILL SETUP
			local nSkillSuccess = 0;
			local nSkillFailure = 0;
			
			-- ITERATE THROUGH EACH SKILL RESULT ROLL
			local nodeRolls = nodeSkill.getChild("rolls");
			if nodeRolls then
				for keyRoll, nodeRoll in pairs(nodeRolls.getChildren()) do
					if NodeManager.get(nodeRoll, "result", "") == "success" then
						nSkillSuccess = nSkillSuccess + 1;
					else
						nSkillFailure = nSkillFailure + 1;
					end
				end
			end
			
			-- SET THE SKILL SUCCESS/FAILURE SUB-TOTALS
			NodeManager.set(nodeSkill, "success", "number", nSkillSuccess);
			NodeManager.set(nodeSkill, "failure", "number", nSkillFailure);

			-- ADD SUB-TOTAL TO OVERALL TOTAL
			nTotalSuccess = nTotalSuccess + nSkillSuccess;
			nTotalFailure = nTotalFailure + nSkillFailure;
		end
	end
	
	-- SET THE OVERALL SUCCESS/FAILURE TOTALS
	local nodeTracker = getTrackerNode();
	NodeManager.set(nodeTracker, "totalsuccess", "number", nTotalSuccess);
	NodeManager.set(nodeTracker, "totalfailure", "number", nTotalFailure);
end

function reset()
	local nodeSkills = getTrackerSkillsNode();
	if nodeSkills then
		for keySkill, nodeSkill in pairs(nodeSkills.getChildren()) do
			NodeManager.set(nodeSkill, "DC", "number", 0);
			
			local nodeRolls = nodeSkill.getChild("rolls");
			if nodeRolls then
				for keyRoll, nodeRoll in pairs(nodeRolls.getChildren()) do
					nodeRoll.delete();
				end
			end

			NodeManager.set(nodeSkill, "success", "number", 0);
			NodeManager.set(nodeSkill, "failure", "number", 0);
		end
	end

	local nodeTracker = getTrackerNode();
	NodeManager.set(nodeTracker, "totalsuccess", "number", 0);
	NodeManager.set(nodeTracker, "totalfailure", "number", 0);
end