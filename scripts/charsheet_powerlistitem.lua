-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	local nodename = getDatabaseNode().getParent().getParent().getName();
	
	local rechargeval = recharge.getValue() or "";
	if rechargeval == "" then
		if nodename == "a0-situational" then
			recharge.setValue("-");
		elseif nodename == "a1-atwill" or nodename == "a2-cantrip" then
			recharge.setValue("At-Will");
		elseif nodename == "a11-atwillspecial" then
			recharge.setValue("At-Will (Special)");
		elseif nodename == "b1-encounter" or nodename == "b2-channeldivinity" then
			recharge.setValue("Encounter");
		elseif nodename == "b11-encounterspecial" then
			recharge.setValue("Encounter (Special)");
		elseif nodename == "c-daily" or nodename == "e-itemdaily" then
			recharge.setValue("Daily");
		end
	end
	
	-- Update the power display
	updateUsageDisplay();
	toggleAbilities();
	toggleDetail();
	
	-- Build our own delete item menu
	registerMenuItem("Delete Item", "delete", 6);
	
	-- Check to see if we should automatically reparse power description
	local nodePower = getDatabaseNode();
	local nParse = NodeManager.get(nodePower, "parse", 0);
	if nParse ~= 0 then
		CharSheetCommon.parseDescription(nodePower);
	end
	
	-- Add menu option to allow manual reparse
	registerMenuItem("Reset power abilities", "textlist", 4);
	
	-- Add menu option to add abilities
	registerMenuItem("Add ability", "pointer", 2);
	registerMenuItem("Add attack/damage ability", "radial_sword", 2, 2);
	registerMenuItem("Add heal ability", "radial_heal", 2, 3);
	registerMenuItem("Add effect ability", "radial_effect", 2, 4);
end

function updateUsageDisplay()
	local mode = windowlist.window.windowlist.window.powermode.getStringValue();
	local nodename = getDatabaseNode().getParent().getParent().getName();
	local flagAWPower = StringManager.contains({"a0-situational", "a1-atwill", "a2-cantrip"}, nodename);

	if mode == "preparation" then
		useatwill.setVisible(false);
		usedcounter.setVisible(false);
		prepared.setVisible(true);
		name.setAnchor("left", "prepared", "right", "absolute", 15);
	elseif flagAWPower then
		useatwill.setVisible(true);
		usedcounter.setVisible(false);
		prepared.setVisible(false);
		name.setAnchor("left", "useatwill", "right", "absolute", 13);
	else
		useatwill.setVisible(false);
		usedcounter.setVisible(true);
		prepared.setVisible(false);
		name.setAnchor("left", "usedcounter", "right", "absolute", 15);
	end
end

function onMenuSelection(selection, subselection)
	if selection == 2 then
		if subselection == 2 then
			local wnd = abilities.createWindow();
			if wnd then
				NodeManager.set(wnd.getDatabaseNode(), "type", "string", "attack");
			end
		elseif subselection == 3 then
			local wnd = abilities.createWindow();
			if wnd then
				NodeManager.set(wnd.getDatabaseNode(), "type", "string", "heal");
			end
		elseif subselection == 4 then
			local wnd = abilities.createWindow();
			if wnd then
				NodeManager.set(wnd.getDatabaseNode(), "type", "string", "effect");
			end
		end
	elseif selection == 4 then
		CharSheetCommon.parseDescription(getDatabaseNode());
	elseif selection == 6 then
		if windowlist.onCheckDeletePermission(self) then
			abilities.reset(false);
			
			windowlist.deleteChild(self, false);
		end
	end
end

function getShortString()
	-- Include the power name
	local str = "Power [" .. name.getValue() .. "]";
	
	-- Add in the action requirement
	local actval = string.lower(action.getValue());
	if actval == "minor" or actval == "minor action" then
		str = str .. " [min]";
	elseif actval == "move" or actval == "move action" then
		str = str .. " [mov]";
	elseif actval == "standard" or actval == "standard action" then
		str = str .. " [std]";
	elseif actval == "free" or actval == "free action" then
		str = str .. " [free]";
	elseif actval == "interrupt" or actval == "immediate interrupt" then
		str = str .. " [imm]";
	elseif actval == "reaction" or actval == "immediate reaction" then
		str = str .. " [imm]";
	end

	-- Return the short string
	return str;
end

function getFullString()
	-- Start with the short string
	local str = getShortString();

	-- Add everything else in the notes
	local shortdesc = NodeManager.get(getDatabaseNode(), "shortdescription", "");
	if shortdesc == "-" then
		shortdesc = "";
	end
	if shortdesc ~= "" then
		str = str .. " - " .. shortdesc;
	end

	-- Return the full string
	return str;
end

function activatePower(showfullstr)
	local desc = "";
	if showfullstr == true then
		desc = getFullString();
	else
		desc = getShortString();
	end
	ChatManager.Message(desc, true, CombatCommon.getActor("pc", getDatabaseNode().getChild(".....")));
end

function setSpacerState()
	if activatedetail.getValue() or activateabilities.getValue() then
		spacer.setVisible(true);
	else
		spacer.setVisible(false);
	end
end

function toggleAbilities()
	local status = activateabilities.getValue();
	abilities.setVisible(status);
	
	for k,v in pairs(abilities.getWindows()) do
		v.updateDisplay();
	end
	
	setSpacerState();
end

function toggleDetail()
	local status = activatedetail.getValue();

	-- Show the power details
	action.setVisible(status);
	recharge.setVisible(status);
	keywords.setVisible(status);
	range.setVisible(status);
	shortdescription.setVisible(status);
	
	-- Set the spacer visibility to match
	setSpacerState();
end
