-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	-- Update the visuals
	updateUsageDisplay();
	abilities.applyFilter();
end

function updateUsageDisplay()
	local nodename = getDatabaseNode().getParent().getParent().getName();
	local flagAWPower = StringManager.contains({"a0-situational", "a1-atwill", "a2-cantrip"}, nodename);

	if flagAWPower then
		useatwill.setVisible(true);
		usedcounter.setVisible(false);
		shortcut.setAnchor("left", "useatwill", "right", "absolute", 1);
	else
		useatwill.setVisible(false);
		usedcounter.setVisible(true);
		shortcut.setAnchor("left", "usedcounter", "right", "absolute", 2);
	end
end

function getShortString()
	-- Include the power name
	local str = "Power [" .. name.getValue() .. "]";
	
	-- Add in the action requirement
	local actval = string.lower(action.getValue());
	if actval == "minor" or actval == "minor action" then
		str = str .. " [min]";
	elseif actval == "move" or actval == "move action" then
		str = str .. " [mov]";
	elseif actval == "standard" or actval == "standard action" then
		str = str .. " [std]";
	elseif actval == "free" or actval == "free action" then
		str = str .. " [free]";
	elseif actval == "interrupt" or actval == "immediate interrupt" then
		str = str .. " [imm]";
	elseif actval == "reaction" or actval == "immediate reaction" then
		str = str .. " [imm]";
	end

	-- Return the short string
	return str;
end

function getFullString()
	-- Start with the short string
	local str = getShortString();

	-- Add everything else in the notes
	local shortdesc = NodeManager.get(getDatabaseNode(), "shortdescription", "");
	if shortdesc == "-" then
		shortdesc = "";
	end
	if shortdesc ~= "" then
		str = str .. " - " .. shortdesc;
	end

	-- Return the full string
	return str;
end

function activatePower(showfullstr)
	local desc = "";
	if showfullstr == true then
		desc = getFullString();
	else
		desc = getShortString();
	end
	ChatManager.Message(desc, true, CombatCommon.getActor("pc", getDatabaseNode().getChild(".....")));
end

function onHover(oncontrol)
	if oncontrol then
		setFrame("sheetfocus", -2, -2, -2, -2);
	else
		setFrame(nil);
	end
end
					
