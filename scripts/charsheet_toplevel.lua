-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	registerMenuItem("Rest", "lockvisibilityon", 8);
	registerMenuItem("Short Rest", "pointer_cone", 8, 8);
	registerMenuItem("Short Rest + Milestone", "pointer_square", 8, 7);
	registerMenuItem("Extended Rest", "pointer_circle", 8, 6);
end

function onMenuSelection(selection, subselection)
	if selection == 8 then
		if subselection == 8 then
			ChatManager.Message("Taking short rest.", true, CombatCommon.getActor("pc", getDatabaseNode()));

			CharSheetCommon.rest(getDatabaseNode());
		elseif subselection == 7 then
			ChatManager.Message("Taking short rest. Milestone reached.", true);

			CharSheetCommon.rest(getDatabaseNode(), false, true);
		elseif subselection == 6 then
			ChatManager.Message("Taking extended rest.", true, CombatCommon.getActor("pc", getDatabaseNode()));

			CharSheetCommon.rest(getDatabaseNode(), true);
		end
	end
end
