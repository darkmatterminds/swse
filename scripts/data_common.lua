-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

-- Conditions supported in power descriptions
-- NOTE: Skipped concealment and cover since there are too many false positives in power descriptions
conditions = {
	"blinded", 
	"dazed", 
	"deafened", 
	"dominated", 
	"grabbed", 
	"immobilized", 
	"insubstantial", 
	"invisible", 
	"marked", 
	"petrified", 
	"phasing",
	"prone", 
	"restrained", 
	"slowed", 
	"stunned", 
	"swallowed", 
	"unconscious", 
	"weakened"
};

-- Effect components which can be targeted
targetableeffectcomps = {
	"ATK",
	"CONC",
	"TCONC",
	"COVER",
	"SCOVER",
	"DEF",
	"AC",
	"FORT",
	"REF",
	"WILL",
	"DMG",
	"IMMUNE",
	"VULN",
	"RESIST"
};

-- Range types supported in power descriptions
rangetypes = {
	"melee",
	"ranged",
	"close",
	"area"
};

-- Damage types supported in power descriptions
dmgtypes = {
	"acid",
	"cold",
	"fire",
	"force",
	"lightning",
	"necrotic",
	"poison",
	"psychic",
	"radiant",
	"thunder"
};

-- Bonus types supported in power descriptions
bonustypes = {
	"racial",
	"power",
	"feat",
	"shield",
	"item",
	"proficiency",
	"enhancement"
};

-- Immunity types supported which are not energy types
immunetypes = {
	"charm",
	"disease",
	"fear",
	"gaze",
	"illusion",
	"petrification",
	"prone",
	"push",
	"pull",
	"sleep",
	"slide"
};

-- Skills supported in power descriptions
skills = {
	"acrobatics",
	"arcana",
	"athletics",
	"bluff",
	"diplomacy",
	"dungeoneering",
	"endurance",
	"heal",
	"history",
	"insight",
	"intimidate",
	"nature",
	"perception",
	"religion",
	"stealth",
	"streetwise",
	"thievery"
};

-- Abilities supported in power descriptions
abilities = {
	"strength",
	"dexterity",
	"constitution",
	"intelligence",
	"wisdom",
	"charisma"
};

-- Skill properties
skilldata = {
	["Acrobatics"] = {
			stat = "dexterity",
			armorcheckmultiplier = 1
		},
	["Athletics"] = {
			stat = "strength",
			armorcheckmultiplier = 1
		},
	["Deception"] = {
			stat = "charisma"
		},
	["Endurance"] = {
			stat = "constitution",
			armorcheckmultiplier = 1
		},
	["Gather Information"] = {
			stat = "charisma"
		},
	["Initiative"] = {
			stat = "dexterity",
			armorcheckmultiplier = 1
		},
	["Knowledge"] = {
			stat = "intelligence"
		},
	["Mechanics"] = {
			stat = "intelligence"
		},
	["Perception"] = {
			stat = "wisdom"
		},
	["Persuasion"] = {
			stat = "charisma"
		},
	["Pilot"] = {
			stat = "dexterity"
		},
	["Stealth"] = {
			stat = "dexterity",
			armorcheckmultiplier = 1
		},
	["Survival"] = {
			stat = "wisdom"
		},
	["Treat Injury"] = {
			stat = "wisdom"
		},
	["Use Computer"] = {
			stat = "intelligence"
		},
	["Use the Force"] = {
			stat = "charisma"
		},
}

