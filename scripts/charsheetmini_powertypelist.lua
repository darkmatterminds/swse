-- 
-- Please see the readme.txt file included with this distribution for 
-- attribution and copyright information.
--

function onInit()
	NodeManager.createChild(getDatabaseNode(), "a0-situational");
	NodeManager.createChild(getDatabaseNode(), "a1-atwill");
	NodeManager.createChild(getDatabaseNode(), "a11-atwillspecial");
	NodeManager.createChild(getDatabaseNode(), "a2-cantrip");
	NodeManager.createChild(getDatabaseNode(), "b1-encounter");
	NodeManager.createChild(getDatabaseNode(), "b11-encounterspecial");
	NodeManager.createChild(getDatabaseNode(), "b2-channeldivinity");
	NodeManager.createChild(getDatabaseNode(), "c-daily");
	NodeManager.createChild(getDatabaseNode(), "d-utility");
	NodeManager.createChild(getDatabaseNode(), "e-itemdaily");
	
	CharSheetCommon.checkForSecondWind(window.getDatabaseNode());
end

function onSortCompare(w1, w2)
	local name1 = w1.getDatabaseNode().getName();
	local name2 = w2.getDatabaseNode().getName();

	if name1 == "" then
		return true;
	elseif name2 == "" then
		return false;
	else
		return name1 > name2;
	end
end

function onFilter(w)
	-- Apply the filter to the individual powers too
	w.powerlist.applyFilter();
	
	-- Always assume combat mode for the powers mini sheet
	local powercount = w.getAvailablePowerCount();
	if powercount <= 0 then
		return false;
	end
	
	return true;
end

